import numpy as np
from deap import algorithms, base, creator, tools

# Evaluation function
def eval_func(individual):
    x, y, z = individual
    result = 1/(1 + (x-2)^2 + (y+1)^2 + (z-1)^2)
    return result,

    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMax)

    toolbox = base.Toolbox()
    toolbox.register("attr_bool", random.randint, -20, 20)

    toolbox.register("individual", tools.initRepeat, creator.Individual,
                     toolbox.attr_float, num = 3)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate", eval_func)
    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1, indpb=0.1)
    toolbox.register("select", tools.selTournament, tournsize=3)

def main():
    population_size = 120
    num_generations = 60

    population = toolbox.population(n=population_size)

    print('\nStarting the evolution process')

    fitnesses = toolbox.map(toolbox.evaluate, population)
    for ind, fit in zip(population, fitnesses):
        ind.fitness.values = fit

    print('\nEvaluated', len(population), 'individuals')

    for generation in range(num_generations):

        print("\n===== Generation", generation)

        offspring = algorithms.varAnd(population, toolbox, cxpb=0.5, mutpb=0.1)

        fitnesses = toolbox.map(toolbox.evaluate, offspring)
        for ind, fit in zip(offspring, fitnesses):
            ind.fitness.values = fit

        population = toolbox.select(offspring, k=population_size)

        top_individual = tools.selBest(population, k=1)[0]
        top_fitness = top_individual.fitness.values[0]

        print(f"Generation {generation}: The best result - {top_fitness:.4f}")

    best_individual = tools.selBest(population, k=1)[0]
    best_fitness = best_individual.fitness.values[0]
    best_params = best_individual[:]

    print(f"\nThe best result: {best_fitness:.4f}")
    print(f"The best pharams: x = {best_params[0]}, y = {best_params[1]}, z = {best_params[2]}")

if name == "main":
    main()



